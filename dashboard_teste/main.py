import pandas as pd
import sqlite3
from tkinter import ttk
from tkinter import *
from pandastable import Table
import platform

conn = sqlite3.connect("C:\\Users\\003868 - SETA\\Desktop\\Projeto Flask\\ies_aps\\app\\investigacao_incidentes.db")

cursor = conn.cursor()

pd.set_option('display.max_rows', None)
pd.set_option('display.expand_frame_repr', False)
pd.set_option('max_colwidth', None)

obra = pd.read_sql('''select obra_codigo || ' - ' || obra_nome as Obra from obra''', conn)

emp = pd.read_sql('''select empresa_nome as empresa from empresa''', conn)

col = pd.read_sql('''select colaborador_nome as colaborador from colaborador''', conn)

tmp = pd.read_sql('''select tempo_funcao as 'Tempo de Função' from colaborador''', conn)

inc_tipo = pd.read_sql('''select incidente_tipo as 'Tipo de Incidente'
                          from incidente''', conn)

cat = pd.read_sql('''select cat as CAT
                     from incidente''', conn)

data_acidente = pd.read_sql(''' select data as 'Data do Acidente'
                                from incidente''', conn)

atividade = pd.read_sql(''' select atividade as 'Atividade Exercida'
                            from incidente''', conn)

afastamento = pd.read_sql('''   select afastamento as 'Houve Afastamento?'
                                from incidente''', conn)

dias_afastamento = pd.read_sql(''' select tempo_dias_afastamento as 'Dias de Afastamento'
                                    from incidente''', conn)

dano_potencial = pd.read_sql(''' select potencial_dano as 'Dano Potencial'
                                from incidente''', conn)

ocorrencia = pd.read_sql(''' select resumo_ocorrencia as 'Resumo da Ocorrência'
                            from incidente''', conn)

causas = pd.read_sql('''select codigo_causas_imediatas || ' - ' || causas_imediatas as 'Causas Imediatas'
                        from tasc_causas_imediatas''', conn)

causas_principais = pd.read_sql('''select codigo_causas_basicas_principais || ' - ' || causas_basicas_principais
                                    as 'Causas Básicas Principais'
                                    from tasc_causas_basicas''', conn)

causas_secundarias = pd.read_sql('''select codigo_causas_basicas_secundarias || ' - ' ||
                                    causas_basicas_secundarias as 'Causas Básicas Secundárias'
                                    from tasc_causas_basicas''', conn)

plano_acao = pd.read_sql('''select plano_acao as 'Plano de Ação' from incidente''', conn)

cursor.fetchall()

d1 = pd.DataFrame(obra)
d2 = pd.DataFrame(emp)
d3 = pd.DataFrame(col)
d4 = pd.DataFrame(tmp)
d5 = pd.DataFrame(inc_tipo)
d6 = pd.DataFrame(cat)
d7 = pd.DataFrame(data_acidente)
d8 = pd.DataFrame(atividade)
d9 = pd.DataFrame(afastamento)
d10 = pd.DataFrame(dias_afastamento)
d11 = pd.DataFrame(dano_potencial)
d12 = pd.DataFrame(ocorrencia)
d13 = pd.DataFrame(causas)
d14 = pd.DataFrame(causas_principais)
d15 = pd.DataFrame(causas_secundarias)
d16 = pd.DataFrame(plano_acao)

data = pd.concat([d1, d2, d3, d4, d5, d6, d7, d8, d9, d10, d11, d12, d13, d14, d15, d16], ignore_index=True, sort=False)

app = Tk()

app.title('teste')

os_info = platform.platform()

###### Obras ######
lista_obras = []

for i in range(len(d1)):
    lista_obras.append(d1.values[i])

list_obra = ttk.Combobox(app,
                         values=lista_obras)

list_obra.pack()

###### Empresa ######

inputtxt = Text(app,
                height=1,
                width=17)

inputtxt.pack()

text_emp = Label(app, text = "")

text_emp.pack()

###### Colaborador ######
input_col = Text(app,
                height=1,
                width=17)

input_col.pack()

text_col = Label(app, text="")

text_col.pack()

###### Tempo de Função ######
lista_tmp = []

for i in range(len(d4)):
    lista_tmp.append(d4.values[i])

list_tmp = ttk.Combobox(app,
                        values=lista_tmp)

list_tmp.pack()

###### Tipo de Incidente ######
lista_tipo_inc = []

for i in range(len(d5)):
    lista_tipo_inc.append(d5.values[i])

list_tipo_inc = ttk.Combobox(app,
                        values=lista_tipo_inc)

list_tipo_inc.pack()

frame = Frame(app)
frame.pack(fill='both', expand=True)

obra_list = Table(frame, dataframe=data)

obra_list.show()

app.mainloop()